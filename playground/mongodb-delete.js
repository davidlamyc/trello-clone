// connect to mongo server
const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        // 'return' so that callback stops execution if connection fails
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    // Delete Many
    db.collection('Todos').deleteMany({ text: 'Eat lunch' }).then((result) => {
        console.log(result);
    });

    // Delete One
    db.collection('Todos').deleteOne({ text: 'Eat lunch' }).then((result) => {
        console.log(result);
    });

    // Find One and Delete
    db.collection('Todos').findOneAndDelete({ completed: false }).then((result) => {
        console.log(result);
    });

    // db.close();
});