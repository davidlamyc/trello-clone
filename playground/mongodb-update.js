// connect to mongo server
const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if (err) {
        // 'return' so that callback stops execution if connection fails
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID('5c09f4b27d311c3bb84b170d')
    }, {
        $set: {
            completed: true
        },
    },
    {
        returnOriginal: false    
    }).then((result) => {
        console.log(result);
    })

    db.close();
});