const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');

var mongoose = require('./db/mongoose');
var { Task } = require('./models/task');
var { User } = require('./models/user');
var { authenticate } = require('./middleware/authenticate');

var app = express();
const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());

app.get('/api/ping', (req, res) => {
    res.send({ ping: "pong" });
})

app.get('/api/auth/ping', authenticate, (req, res) => {
    res.send({ ping: "auth pong" });
})

app.post('/api/tasks', authenticate, (req, res) => {
    var task = new Task({
        text: req.body.text,
        status: req.body.number,
        subtasks: req.body.subtasks,
        _creator: req.user._id
    });

    task.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    })
})

app.get('/api/tasks', authenticate, (req, res) => {
    Task.find({ 
        _creator: req.user._id
    }).then((tasks) => {
        res.send({tasks})
    }, (e) => {
        res.status(400).send(e);
    })
})

app.get('/api/tasks/:id', authenticate, (req, res) => {
    var id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Task.findOne({
        _id: id,
        _creator: req.user._id
    }).then((task) => {
        if (!task) {
            return res.status(404).send();
        }

        res.send({ task });
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.delete('/api/tasks/:id', authenticate, (req, res) => {
    var id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(400).send();
    }

    Task.findOneAndRemove({
        _id: id,
        _creator: req.user._id
    }).then((task) => {
        if (!task) {
            return res.status(404).send();
        }

        res.send({ task });
    }).catch((e) => {
        res.status(400).send(e);
    });
})

app.patch('/api/tasks/:id', authenticate, (req, res) => {
    var id = req.params.id;

    // pick takes an object, and picks off the properties that exist in below array
    var body = _.pick(req.body, ['text', 'status', 'subtasks']);

    if (!ObjectID.isValid(id)) {
        res.status(400).send();
    }

    // if (_.isBoolean(body.completed) && body.completed) {
    //     body.completedAt = new Date().getTime();
    // } else {
    //     body.completed = false;
    //     body.completedAt = null;
    // }

    Task.findOneAndUpdate({_id: id, _creator: req.user._id}, {$set: body}, {new: true}).then((task) => {
        if (!task) {
            return res.status(400).send();
        }

        res.send({task});
    }).catch((e) => {
        res.status(400).send();
    })
})

app.post('/api/users', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch((e) => {
        res.status(400).send(e);
    })
})

app.get('/api/users/me', authenticate, (req, res) => {
    res.send(req.user);
})

app.post('/api/users/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        user.generateAuthToken().then((token) => {
            res.header('x-auth', token).send(user);
        })
    }).catch((e) => {
        res.status(400).send(e);
    });
});

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'));

    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    })
}

app.listen(PORT, () => {
    console.log(`Started up at post ${PORT}`);
})
