var mongoose = require('mongoose');
var keys = require('../config/keys');

mongoose.Promise = global.Promise;
mongoose.connect(keys.mongoURI || 'mongodb://localhost:27017/TodoApp');

module.exports = { 
    mongoose 
};