var mongoose = require('mongoose');

var Task = mongoose.model('Task', {
    text: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    status: {
        type: Number,
        default: 0
    },
    subtasks: {
        type: Array
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});



module.exports = { Task };