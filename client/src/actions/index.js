import axios from 'axios';
import { LOGIN_USER,
    LOGOUT_USER, 
    REGISTER_USER,
    CREATE_TASK, 
    GET_TASKS, 
    COMPLETE_TASK, 
    DELETE_TASK
} from './types';

export const fetchUser = () => async dispatch =>  {
    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.get('/api/users/me', {headers: {"x-auth": token}});

    dispatch({type: LOGIN_USER, payload: { 
        email: response.data.email,
        _id: response.data._id
        }
    })
}

export const createTask = (task, subtasks) => async dispatch => {
    var subtasksArray = [];

    var subtasksLength;
    if (subtasks) {
        subtasksLength = subtasks.length;

        for (var i = 0; i < subtasksLength; i++) {
            subtasksArray.push(Object.assign({}, {
                "text" : subtasks[i],
                "completed": false
            }));
        }
    }

    var newTaskObject = Object.assign({}, {"text" : task}, {"subtasks": subtasksArray});

    var token = localStorage.getItem('trelloCloneToken');

    try {
        const response = await axios.post('/api/tasks', 
            newTaskObject,
            { headers: {"x-auth": token} }
        );
        if (response.status === 200) {
            dispatch({type: CREATE_TASK, payload: { 
                task: response.data
                }
            })
        }
        return response.status;
    } catch (error) {
        return error;
    } 
    
}

export const userLogin = (email, password) => async dispatch => {
    var loginDetails = {email: email, password: password}
    try {
        const response = await axios.post('/api/users/login', loginDetails);
        localStorage.setItem('trelloCloneToken', response.headers['x-auth']);
        dispatch({type: LOGIN_USER, payload: { 
            email: response.data.email,
            _id: response.data._id
            }
        })
        return response.status;
    } catch (error) {
        return error;
    } 
}

export const userRegister = (email, password) => async dispatch => {
    var registerDetails = {email: email, password: password}
    try {
        const response = await axios.post('/api/users', registerDetails);
        localStorage.setItem('trelloCloneToken', response.headers['x-auth']);

        dispatch({type: REGISTER_USER, payload: { 
            email: response.data.email,
            _id: response.data._id
            }
        })
        return response.status;
    } catch (error) {
        return error;
    } 
}

export const userLogout = () => async dispatch => {
    localStorage.removeItem('trelloCloneToken');
    dispatch({type: LOGOUT_USER, payload: { 
        email: '',
        _id: ''
        }
    })
}

// Here


export const fetchTasks = () => async dispatch => {
    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.get('/api/tasks', 
        { headers: {"x-auth": token} }
    );
    dispatch({type: GET_TASKS, payload: { 
        tasksArray: response.data.tasks,
        }
    })
}

export const switchSubtask = (subtask, thisTask) => async dispatch => {
    for (var i = 0; i < thisTask.subtasks.length; i++) {
        if (thisTask.subtasks[i].text === subtask.text) {
            thisTask.subtasks[i].completed = !thisTask.subtasks[i].completed;
        };
    }

    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.patch(`/api/tasks/${thisTask._id}`, 
        { "subtasks": thisTask.subtasks },
        { headers: {"x-auth": token} }
    );
    if (response.status === 200) {
        dispatch({type: COMPLETE_TASK, payload: { 
            task: response.data.task
            }
        })
    }
}

// TODO: FIX DELETE SUBTASK FEATURE
// export const deleteSubtask = (subtask, theTask) => async dispatch => {
//     var subtaskIndex;
//     for (var i = 0; i < theTask.subtasks.length; i++) {
//         if (theTask.subtasks[i].text === subtask.text){
//             subtaskIndex = i;
//         }
//     }

//     theTask.subtasks.splice(subtaskIndex, 1);

//     var token = localStorage.getItem('trelloCloneToken');
//     const response = await axios.patch(`/api/tasks/${theTask._id}`, 
//         { "subtasks": theTask.subtasks },
//         { headers: {"x-auth": token} }
//     );

//     // TODO: FIX SUBTASK DELETE LAG
//     // if (response.status === 200) {
//     //     dispatch({type: COMPLETE_TASK, payload: { 
//     //         task: response.data.task
//     //         }
//     //     })
//     // }
//     // return response.status;
// }

export const addSubtask = (subtask, thisTask) => async dispatch => {
    thisTask.subtasks.push({"text": subtask, "completed": false});

    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.patch(`/api/tasks/${thisTask._id}`, 
        { "subtasks": thisTask.subtasks },
        { headers: {"x-auth": token} }
    );
    if (response.status === 200) {
        dispatch({type: COMPLETE_TASK, payload: { 
            task: response.data.task
            }
        })
    }
    return response.status;
}

export const completeTask = (id, status) => async dispatch => {
    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.patch(`/api/tasks/${id}`, 
        { "status": status },
        { headers: {"x-auth": token} }
    );
    if (response.status === 200) {
        dispatch({type: COMPLETE_TASK, payload: { 
            task: response.data.task
            }
        })
    }
}

export const deleteTask = (id) => async dispatch => {
    var token = localStorage.getItem('trelloCloneToken');
    const response = await axios.delete(`/api/tasks/${id}`, 
        { headers: {"x-auth": token} }
    );
    if (response.status === 200) {
        dispatch({type: DELETE_TASK, payload: { 
            task: response.data.task
            }
        })
    }
}
