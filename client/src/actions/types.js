export const CREATE_TASK = 'create_task';
export const LOGIN_USER = 'login_user';
export const REGISTER_USER = 'register_user';
export const LOGOUT_USER = 'logout_user';
export const GET_TASKS = 'get_tasks';
export const COMPLETE_TASK = 'complete_task';
export const DELETE_TASK = 'delete_task';