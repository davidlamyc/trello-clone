import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createTask } from '../actions';

class CreateTaskForm extends Component {
    state = { 
        task: '',
        subtaskInput: '',
        subtaskExistMessage: '',
        createTaskErrorMessage: '',
        subtasks: []
    };

    onTaskCreateSubmit = event => {
        event.preventDefault();
        this.props.createTask(this.state.task, this.state.subtasks)
            .then((status) => {
                if (status === 200){
                    this.setState({ task: '', subtaskInput: '', subtasks: [] })
                    this.setState({createTaskErrorMessage: ''})
                } else {
                    this.setState({createTaskErrorMessage: "Task was not created. Please try again."})
                }
            })
    }

    render(){
        return (
            <div className="row">
                <form className="col col s10 push-s1" onSubmit={this.onTaskCreateSubmit}>
                    <div className="row">
                        <div className="input-field col s6">
                            <input 
                                id="task"
                                type="text" 
                                className="validate"
                                onChange={(e) => this.setState({ task: e.target.value })}
                                value={this.state.task}
                            />
                            <label htmlFor="task" className="active">Add Task</label>
                        </div>
                        <div className="input-field col s6">
                            <input 
                                id="subtask"
                                type="text" 
                                className="validate"
                                onChange={
                                    (e) => {
                                        this.setState({ subtaskInput: e.target.value });
                                        for (var i = 0; i < this.state.subtasks.length; i++) {
                                            if (this.state.subtasks[i] == e.target.value) {
                                                this.setState({ subtaskExistMessage: 'Subtask already exists.' })
                                            } else {
                                                this.setState({ subtaskExistMessage: '' })
                                            }
                                        }
                                    }
                                }
                                value={this.state.subtaskInput}      
                            />
                            <span class="helper-text" data-error="wrong" data-success="right">{this.state.subtaskExistMessage}</span>

                            <div>
                                <span className="badge new" data-badge-caption="+" onClick={() => {
                                    this.setState({
                                        subtasks: [...this.state.subtasks, this.state.subtaskInput]
                                    });
                                    this.setState({ subtaskInput: ''});
                                }   
                                }></span>
                            </div>
                            <label htmlFor="subtask" className="active">Add Subtask</label>
                        
                        </div>

                        <div className="input-field col s12">
                            <label className="active">Subtasks</label>
                            {
                                this.state.subtasks.map((s) => {
                                    return (
                                    <p className="col s6">
                                        {s}
                                        <span key={s} className="badge new orange" data-badge-caption="REMOVE" onClick={
                                            () => {
                                                var tempSubtasksArray = this.state.subtasks.filter((subtask) => {
                                                    return subtask != s;
                                                })
                                                this.setState({ subtasks: tempSubtasksArray })
                                            }
                                        }
                                        ></span>
                                    </p>  
                                    ) 
                                })
                            }
                        </div>
                           
                        <button className="waves-effect waves-light btn-small">Create Task</button>
                        <p>
                            <span class="helper-text" data-error="wrong" data-success="right">{this.state.createTaskErrorMessage}</span>
                        </p>
                    </div>
                </form>
            </div>       
        )
    }
}

const mapStateToProps = (state) => {
    return { tasks: state.tasks, auth: state.auth }
}

export default connect(mapStateToProps, {createTask})(CreateTaskForm); 


