import React from 'react';
import LoginForm from './LoginForm';

const Landing = () => {
    return (
        <div style={{ textAlign: 'center' }}>
            <h1>
                A Trello Clone App
            </h1>
            <p> An minimalist workflow tool to help you keep track of your workload. </p>
            <p> Inspired by the ever-awesome <a href="https://trello.com">Trello</a>.</p>
            <LoginForm/>
        </div>
    )
}

export default Landing;