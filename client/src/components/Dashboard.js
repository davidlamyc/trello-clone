import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

// import CreateTaskForm from './CreateTaskForm';

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.STinput = React.createRef();
    }

    state = { 
        task: '',
        subtask: ''
    };

    componentDidMount() {
        this.props.fetchTasks();
    }

    // TODO: Refactor completeTask in actions
    onMoveTask = (id, status) => {
        this.props.completeTask(id, status);
    }

    onSwitchSubtask = (subtask, task) => {
        this.props.switchSubtask(subtask,task);
    }

    onDeleteTask = id => {
        this.props.deleteTask(id);
    }

    onTaskCreateSubmit = event => {
        event.preventDefault();
        this.props.createTask(this.state.task.toUpperCase(), this.state.subtasks)
            .then((status) => {
                if (status === 200){
                    this.setState({ task: '' });
                } else {
                    
                }
            })
    }

    onCreateSubtask = (event, thisTask) => {
        this.props.addSubtask(this.STinput.current.value, thisTask)
            .then((status) => {
                if (status === 200){
                    this.STinput.current.value = '';
                }
            })
        event.preventDefault();
    }

    onAddSubtask = (event, thisTask) => {
        event.preventDefault();
        this.props.addSubtask(this.state.subtask, thisTask)
            .then((status) => {
                if (status === 200){
                    this.setState({ subtask: '' })
                } else {
                    this.setState({createTaskErrorMessage: "Task was not created. Please try again."})
                }
            })
    }

    // onDeleteSubtask = (s, innerTask) => {
    //     this.props.deleteSubtask(s, innerTask);
    // }

    render() {

        // TO DO JSX HANDLER
        if (this.props.tasks){
            var todoList = this.props.tasks.tasksArray
                .filter((t) => {return t.status == 0})
                .map((t)=> {
                    var innerTask = t;
                    return (
                        <div key={t._id} className="card grey darken-2" > 
                            <div className="card-content white-text">
                                <h6 className="card-title white-text">{t.text}</h6>
                                    <form action="#" className="white-text">
                                        {
                                            t.subtasks.map((s) => {
                                            var isDone = s.completed ? "checked": "";
                                            return (
                                                <div>
                                                    <label>
                                                        <input readOnly type="checkbox" checked={isDone} onClick={()=>this.onSwitchSubtask(s, innerTask)}/>
                                                        <span>{s.text}</span>        
                                                    </label>
                                                    {/* <i className="material-icons icon-red right" onClick={()=>this.onDeleteSubtask(s, innerTask)}>clear</i> */}
                                                    <br/>
                                                </div>
                                                ) 
                                            })
                                        }
                                        <form action="#" className="white-text" onSubmit={(event) => this.onCreateSubtask(event, innerTask)}>
                                            <input 
                                                id="subtask"
                                                type="text" 
                                                className="validate"
                                                ref={this.STinput}
                                                className="white-text"
                                            />
                                            <label htmlFor="subtask" className="active">Add Subtask</label>
                                        </form>
                                    </form>
                            </div>
                            <div className="card-action">
                                <i className="material-icons icon-orange small" onClick={()=>this.onMoveTask(t._id, 1)}>arrow_forward</i>
                                <i className="material-icons icon-red small" onClick={()=>this.onDeleteTask(t._id)} >delete_forever</i>
                            </div>
                        </div>
                    )
                })
        }

        // DOING JSX HANDLER
        if (this.props.tasks){
            var doingList = this.props.tasks.tasksArray
                .filter((t) => {return t.status == 1})
                .map((t)=> {
                    var innerTask = t;
                    return (
                        <div key={t._id} className="card blue-grey darken-2" > 
                            <div className="card-content white-text">
                                <h6 className="card-title white-text">{t.text}</h6>
                                    <form action="#" className="white-text">
                                        {
                                            t.subtasks.map((s) => {
                                            var isDone = s.completed ? "checked": "";
                                            return (
                                                <div>
                                                    <label>
                                                        <input readOnly type="checkbox" checked={isDone} onClick={()=>this.onSwitchSubtask(s, innerTask)}/>
                                                        <span>{s.text}</span>        
                                                    </label>
                                                    <br/>
                                                </div>
                                                ) 
                                            })
                                        }
                                        <form action="#" className="white-text" onSubmit={(event) => this.onCreateSubtask(event, innerTask)}>
                                            <input 
                                                id="subtask"
                                                type="text" 
                                                className="validate"
                                                ref={this.STinput}
                                                className="white-text"
                                            />
                                            <label htmlFor="subtask" className="active">Add Subtask</label>
                                        </form>
                                    </form>
                            </div>
                            <div className="card-action">
                                <i className="material-icons icon-red small" onClick={()=>this.onDeleteTask(t._id)} >delete_forever</i>
                                <i className="material-icons icon-orange small" onClick={()=>this.onMoveTask(t._id, 2)} >arrow_forward</i>
                                <i className="material-icons icon-orange small" onClick={()=>this.onMoveTask(t._id, 0)} >arrow_backward</i>
                            </div>
                        </div>
                    )
                })
        }

        // DONE JSX HANDLER
        if (this.props.tasks){
            var doneList = this.props.tasks.tasksArray
                .filter((t) => {return t.status == 2})
                .map((t)=> {
                    var innerTask = t;
                    return (
                        <div key={t._id} className="card teal darken-3"> 
                            <div className="card-content white-text">
                                <h6 className="card-title white-text">{t.text}</h6>
                                    <form action="#" className="white-text">
                                        {
                                            t.subtasks.map((s) => {
                                            var isDone = s.completed ? "checked": "";
                                            return (
                                                <div>
                                                    <label>
                                                        <input readOnly type="checkbox" checked={isDone} onClick={()=>this.onSwitchSubtask(s, innerTask)}/>
                                                        <span>{s.text}</span>        
                                                    </label>
                                                    <br/>
                                                </div>
                                                ) 
                                            })
                                        }
                                        <form action="#" className="white-text" onSubmit={(event) => this.onCreateSubtask(event, innerTask)}>
                                            <input 
                                                id="subtask"
                                                type="text" 
                                                className="validate"
                                                ref={this.STinput}
                                                className="white-text"
                                            />
                                            <label htmlFor="subtask" className="active">Add Subtask</label>
                                        </form>
                                    </form>
                            </div>
                            <div className="card-action">
                            <i className="material-icons icon-red small" onClick={()=>this.onDeleteTask(t._id)} >delete_forever</i>
                            <i className="material-icons icon-orange small" onClick={()=>this.onMoveTask(t._id, 1)} >arrow_backward</i>
                            </div>
                        </div>
                    )
                })
        }
        
        return (
            <div>
                <div className="row">

                    <div className="col s12 l4">
                        <div className="card grey darken-1">
                            <div className="card-content white-text">
                                <span className="card-title">To Do</span>
                            </div>
                            <div className="card-action">
                                {todoList}
                                <div className="card grey darken-2">
                                    <div className="card-content">
                                        <form action="#" className="white-text" onSubmit={(event) => this.onTaskCreateSubmit(event)}>
                                            <input 
                                                id="newtask"
                                                type="text" 
                                                className="validate"
                                                onChange={(e) => this.setState({ task: e.target.value })}
                                                value={this.state.task}
                                                className="white-text"
                                            />
                                            <label htmlFor="newtask" className="active">New Task</label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div className="col s12 l4">
                        <div className="card blue-grey darken-1">
                            <div className="card-content white-text">
                                <span className="card-title">Doing</span>
                            </div>
                            <div className="card-action">
                                {doingList}
                            </div>
                        </div>
                    </div>

                    

                    <div className="col s12 l4">
                        <div className="card teal darken-1">
                            <div className="card-content white-text">
                                <span className="card-title">Done</span>
                            </div>
                            <div className="card-action">
                                {doneList}
                            </div>
                        </div>
                    </div>

                </div>

                {/* <CreateTaskForm/> */}

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { tasks: state.tasks }
}

export default connect(mapStateToProps, actions)(Dashboard);