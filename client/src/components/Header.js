import React, { Component } from 'react';
import { connect } from 'react-redux';

import { userLogout } from '../actions';

class Header extends Component {
    renderContent() {
        switch (this.props.auth) {
            case null:
                if (window.location.href.indexOf("register") > -1){
                    return <li><a href="/" className="waves-effect waves-light btn-small">Back to Login</a></li>
                }
                return <li><a href="/register" className="waves-effect waves-light btn-small">Register</a></li>
            case false:
                if (window.location.href.indexOf("register") > -1){
                    return <li><a href="/" className="waves-effect waves-light btn-small">Back to Login</a></li>
                }
                return <li><a href="/register" className="waves-effect waves-light btn-small">Register</a></li>
            default:
                return <li><a href="/" onClick={this.onLogout}>Logout</a></li>;
        }   
    }

    onLogout = () => {
        this.props.userLogout();
    }

    render() {
        return (
            <nav>
                <div className="nav-wrapper">
                    <a className="brand-logo">A Trello Clone</a>
                    <ul className="right">
                        {this.renderContent()}
                    </ul>
                </div>
            </nav>
        );
    }
}

function mapStateToProps({ auth }) {
    return { auth };
}

export default connect(mapStateToProps, {userLogout})(Header);