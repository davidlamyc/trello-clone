import './App.css'
import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Header from './Header';
import Landing from './Landing';
import Dashboard from './Dashboard';
import Register from './Register';

class App extends Component {

  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Header />
            <div>
              <Route exact path="/" component={Landing}/>
              <Route exact path="/dashboard" component={Dashboard} />
              <Route exact path="/register" component={Register} />
            </div>
          </div>
        </BrowserRouter>
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return { auth: state.auth }
}

export default connect(mapStateToProps, actions)(App);
