import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { userLogin } from '../actions';

class LoginForm extends Component {
    state = { 
        email: '',
        password: '',
        errorMessage: ''
    };

    onFormSubmit = event => {
        event.preventDefault();
        this.props.userLogin(this.state.email, this.state.password)
            .then((status) => {
                if (status === 200){
                    this.setState({errorMessage: ""})
                    this.setState({ 
                        email: '',
                        password: '' 
                    })
                } else {
                    this.setState({errorMessage: "Email and password combination does not exist. Please try again."})
                }
            })
    }

    render(){
        if (this.props.auth){
            return <Redirect to="/dashboard"/>
        }
        return (
            <div className="container">
                <div className="row">
                    <form className="col col s8 push-s2" onSubmit={this.onFormSubmit}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input 
                                    id="email"
                                    type="email" 
                                    className="validate"
                                    onChange={(e) => this.setState({ email: e.target.value })}
                                    value={this.state.email}
                                />
                                <label htmlFor="email" className="active">Email</label>
                            </div>
                            <div className="input-field col s12">
                                <input 
                                    id="password" 
                                    type="password" 
                                    className="validate"
                                    onChange={(e) => this.setState({ password: e.target.value })}
                                    value={this.state.password}
                                />
                                <label htmlFor="password" className="active">Password</label>
                            </div>
                            <p>{this.state.errorMessage}</p>
                            <button className="waves-effect waves-light btn-small">Login</button>
                        </div>
                    </form>
                </div>  
            </div>     
        )
    }
}

const mapStateToProps = (state) => {
    return { auth: state.auth }
}

export default connect(mapStateToProps, {userLogin})(LoginForm); 


