import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { userRegister } from '../actions';

class Register extends Component {
    state = { 
        registerEmail: '',
        registerPassword: '',
        registerErrorMessage: ''
    };

    onRegisterFormSubmit = event => {
        event.preventDefault();
        this.props.userRegister(this.state.registerEmail, this.state.registerPassword)
            .then((status) => {
                if (status === 200){
                    this.setState({
                        registerErrorMessage: '',
                        registerEmail: '',
                        registerPassword: ''
                    })
                } else {
                    this.setState({registerErrorMessage: "Something went wrong. Please try again."})
                }
            })
    }

    render(){
        if (this.props.auth){
            return <Redirect to="/dashboard"/>
        }
        return (
            <div className="container">
                <div style={{ textAlign: 'center' }}>
                    <h3>
                        Get started in a jiffy.
                    </h3>
                    <p> Please enter an email in <b>a valid format</b>, and a password with <b>6 or more characters.</b>.</p>
                </div>
                <div style={{ textAlign: 'center' }} className="card blue-grey darken-2">
                    <div className="card-content white-text">
                        <h1 className="card-title">Register</h1>
                        <form className="col col s8 push-s2" onSubmit={this.onRegisterFormSubmit}>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input 
                                        id="email"
                                        type="email" 
                                        className="validate"
                                        onChange={(e) => this.setState({ registerEmail: e.target.value })}
                                        value={this.state.registerEmail}
                                    />
                                    <label htmlFor="email" className="active">Email</label>
                                </div>
                                <div className="input-field col s12">
                                    <input 
                                        id="password" 
                                        type="password" 
                                        className="validate"
                                        onChange={(e) => this.setState({ registerPassword: e.target.value })}
                                        value={this.state.registerPassword}
                                    />
                                    <label htmlFor="registerPassword" className="active">Password</label>
                                </div>
                                <p>{this.state.registerErrorMessage}</p>                                    <button className="waves-effect waves-light btn-small">Register</button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>      
        )
    }
}

const mapStateToProps = (state) => {
    return { auth: state.auth }
}


export default connect(mapStateToProps, {userRegister})(Register); 


