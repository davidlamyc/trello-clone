import { LOGIN_USER, LOGOUT_USER, REGISTER_USER } from '../actions/types';

export default function(state = null, action) {

    switch (action.type) {
        case LOGIN_USER:
            return action.payload || false;
        case LOGOUT_USER:
            return action.payload || false;
        case REGISTER_USER:
            return action.payload || false;
        default:
            return state;
    }
}