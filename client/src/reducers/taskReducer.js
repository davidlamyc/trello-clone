import { CREATE_TASK, GET_TASKS, COMPLETE_TASK, DELETE_TASK } from '../actions/types';

export default function(state = null, action) {

    switch (action.type) {

        case CREATE_TASK:
            var newCreateTasksArray = {
                "tasksArray": [...state.tasksArray, action.payload.task]
            }
            return newCreateTasksArray;

        case COMPLETE_TASK:
            var tempArray = [...state.tasksArray];
            var completedTaskIdx = tempArray.findIndex((task) => {
                return task._id === action.payload.task._id;
            });

            tempArray.splice(completedTaskIdx, 1, action.payload.task);
            var returnArray = {
                "tasksArray": tempArray
            }
            return returnArray;

        case GET_TASKS:
            return action.payload || false;

        case DELETE_TASK:
            var afterRemovalTasksArray = state.tasksArray.filter((task) => {
                return task._id !== action.payload.task._id;
            });
            var newTasksArray = {
                "tasksArray": afterRemovalTasksArray
            }
            return newTasksArray;
            
        default:
            return state;
    }
} 