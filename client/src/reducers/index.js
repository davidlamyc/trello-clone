import { combineReducers } from 'redux';
import authReducer from './authReducer';
import tasksReducer from './taskReducer';

export default combineReducers({
    auth: authReducer,
    tasks: tasksReducer
});